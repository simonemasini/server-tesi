/**
 * Created by Simone Masini
 */
var mysql = require('mysql');
var express = require('express');
var bodyParser = require('body-parser');
//var rstats  = require('rstats');

var exec = require('child_process').exec;

var app = express();

app.use(bodyParser.urlencoded(
    { extended: false,
        limit: '50mb'
    }
));

app.use(bodyParser.json({limit: '50mb'}));

var connection;
handleDisconnect();

var server_port = 1337;

var server = app.listen(server_port, function () {

    var host = server.address().address;
    var port = server.address().port;
    console.log('Server run at http://%s:%s', host, port)

});

app.use(function (req, res) {
    switch(req.url) {
        case '/insert':
            insertHandler(req, res);
            break;
        case '/requestValidSensor':
            requestValidSensorHandler(req, res);
            break;
        case '/insertUser':
            insertUser(req, res);
            break;
        case '/login':
            login(req, res);
            break;
    };
});

function requestValidSensorHandler(req,res){

    if (req.method == 'POST') {
        var sensorType = req.body.sensorType;
        //controllo nel db se esite
        querySensor(sensorType,res);
    }
}

function insertData(insert, table, res, countInsert,countLocal){
    connection.query('INSERT INTO '+table+' SET ?', insert, function(err, result) {
        if(err)	{
            res.write("no");
            res.end();
        }else{
            console.log(result);
            if(res){
                res.write("yes");
            }
        }
        if(countInsert==countLocal || res){
            res.end();
        }
    });
}

function querySensor(type, res, insert, countInsert,countLocal){
    var strQuery = "select IDSensor from sensor where Type='"+type+"'";
    connection.query(strQuery, function(err, rows){
        if(err)	{
            throw err;
        }else{
            var respond = "no";
            if(rows[0]){
                respond=rows[0]["IDSensor"];
            }
            console.log("risposta dal server sul sensore:"+respond);
            if(insert){
                insert["IDSensor"]=respond;
                insertData(insert, "readdata",res, countInsert,countLocal);
            }else{
                res.write(""+respond);
                res.end();
            }
        }
    });
}

function insertUser(req, res){
    if (req.method == 'POST') {
        var obj = req.body;
        //inserimento con json
        insertData(obj,"user",res);
    }
}

function insertHandler(req, res){
    console.log("Inserimento");
    if (req.method == 'POST') {
        var obj = req.body;
        var strQuery = "select MAX(IDRead) from readdata";
        connection.query(strQuery, function(err, rows){
            if(err)	{
                throw err;
            }else{
                var id=0;
                if(rows[0]){
                    id=rows[0]["MAX(IDRead)"]
                }
                var countInsert = obj.values.length;
                var countLocal = 0;
                for(var k in obj.values){
                    countLocal++;
                    id++;
                    var type = obj["values"][k]["type"];
                    var dataS = obj["values"][k]["data"];
                    var lat = obj["values"][k]["lat"];
                    var lng = obj["values"][k]["lng"];
                    var date = obj["values"][k]["date"];
                    var time = obj["values"][k]["time"];
                    var user = obj["values"][k]["user"];
                    var insert = {
                        IDRead: id,
                        IDSensor: type,
                        Latitude: lat,
                        Longitude: lng,
                        Data: dataS,
                        Date: date,
                        Time: time,
                        User: user
                    };
                    querySensor(type,res,insert, countInsert,countLocal);
                }
            }
        });
    }
}

function login(req, res){
    if (req.method == 'POST') {
        var obj = req.body;
        var strQuery = "select COUNT(*) from user where username='"+obj["user"]+"' AND password='"+obj["pass"]+"'";
        connection.query(strQuery, function(err, rows){
            if(err)	{
                throw err;
            }else{
                if(rows[0]["COUNT(*)"]==1){
                    console.log("login corretto");
                    res.write("ok");
                }else{
                    console.log("login errato");
                    res.write("no");
                }
                res.end();
            }
        });
    }
}

function handleDisconnect() {
    /*connection = mysql.createConnection({
        host : "phpmyadmin.web.cs.unibo.it",
        user : "my1503",
        password: "rateizee",
        database: "my1503"
    });*/
    connection = mysql.createConnection({
        host : "localhost",
        user : "root",
        password: "",
        database: "sensordata"
    });

    connection.connect(function(err) {
        if(err) {
            console.log('error when connecting to db:', err);
            setTimeout(handleDisconnect, 2000);
        }else{
            console.log('DB connection establish');
        }
    });

    connection.on('error', function(err) {
        console.log('db error', err);
        if(err.code === 'PROTOCOL_CONNECTION_LOST') {
            handleDisconnect();
        } else {
            throw err;
        }
    });
}
